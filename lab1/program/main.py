import numpy as np
from numpy.fft import rfft, rfftfreq
import math
import matplotlib.pyplot as plt
from myfunc import myfunc
import matplotlib as mpl


# Функции для шрифтов(Не сильно важные вещи)
mpl.rcParams['font.family'] = 'fantasy'
mpl.rcParams['font.fantasy'] = 'Comic Sans MS, Arial'

ORIGIN_FREQUENCY = 60
DISCRET_FREQUENCY = 30
START_TIME = 0
END_TIME = 2
ERROR_STEP = 0.1

# Построение массива точек заданного графика
def calculateMyfuncGraph(values, timePoints, frequency, startTime, endTime):
    step = 1.0 / frequency
    for i in np.arange(startTime, endTime+step, step):
        timePoints.append(i)
        values.append(myfunc(i))



def sinc(x):
    if x == 0:
        return 1
    else:
        return math.sin(x) / x


# Функция Котельникова
def funcShin(t, values, frequency):
    step = 1.0 / frequency
    val = 0.0
    for i in range(0, len(values)):
        val += values[i] * sinc(math.pi * (t - i * step) / step)
    return val

# Построение восстановленного графика (масива точек)
def restoreGraph(values, timePoints, frequency, startTime, endTime, discretValues, discretFrequency):
    step = 1.0 / frequency
    for i in np.arange(startTime, endTime+step, step):
        timePoints.append(i)
        values.append(funcShin(i,discretValues, discretFrequency))

# График ошибок
def errorGraph(originValues, restoreValues):
    returnValues = []
    for i in range(0, len(originValues)):
        returnValues.append((originValues[i] - restoreValues[i]) ** 2)
    return returnValues

# Величина ошибки
def errorValue(originValues, restoreValues, startTime, endTime):
    v = errorGraph(originValues, restoreValues)
    sum = 0.0
    for i in range(0, len(v)):
        sum += v[i]
    duration = endTime - startTime
    return sum*(1/duration)

# Аналоговый график
plt.figure()
values = []
timePoints = []
calculateMyfuncGraph(values, timePoints, ORIGIN_FREQUENCY, START_TIME, END_TIME)
plt.plot(timePoints, values, 'b')
plt.xlabel('Время, сек')
plt.ylabel('Амплитуда сигнала')
plt.grid()
#plt.title('Аналоговый"Син.", Дискретный"Зел.", Восстановленный график"Крас."')


# Дискретный график
plt.figure()
discretValues = []
discretTimePoints = []
calculateMyfuncGraph(discretValues, discretTimePoints, DISCRET_FREQUENCY, START_TIME, END_TIME)
plt.stem(discretTimePoints, discretValues, 'g')
plt.xlabel('Время, сек')
plt.ylabel('Амплитуда сигнала')

# Восстановленный график
plt.figure()
newValues = []
newTimePoints = []
restoreGraph(newValues, newTimePoints, ORIGIN_FREQUENCY, START_TIME, END_TIME, discretValues, DISCRET_FREQUENCY)
plt.plot(newTimePoints, newValues, 'r')
plt.plot(timePoints, values, 'b')
plt.grid()
plt.xlabel('Время, сек')
plt.ylabel('Амплитуда сигнала')

# График ошибок
plt.figure()
errorValues = errorGraph(values, newValues)
errorSum = errorValue(values, newValues, START_TIME, END_TIME)
print("Величина ошибки: ", errorSum)
plt.stem(timePoints, errorValues)
plt.xlabel('Время, сек')
plt.ylabel('Квадрат ошибки E(t)')
#plt.title('График квадрата ошибок')
plt.grid()

# Зависимость ошибки от частоты дискретизации
plt.figure()
frequencyValues = []
errorValues = []
step = ERROR_STEP
for i in np.arange(1, ORIGIN_FREQUENCY, step):
    values = []
    timePoints = []
    calculateMyfuncGraph(values, timePoints, ORIGIN_FREQUENCY, START_TIME, END_TIME)
    discretValues = []
    discretTimePoints = []
    calculateMyfuncGraph(discretValues, discretTimePoints, i, START_TIME, END_TIME)
    newValues = []
    newTimePoints = []
    restoreGraph(newValues, newTimePoints, ORIGIN_FREQUENCY, START_TIME, END_TIME, discretValues, i)
    errorVal = errorValue(values, newValues, START_TIME, END_TIME)
    frequencyValues.append(i)
    errorValues.append(errorVal)
plt.plot(frequencyValues, errorValues)
plt.xlabel('Частота дискретизации')
plt.ylabel('Ошибка')
#plt.title('Зависимость ошибки от частоты дискретизации')
plt.grid()

# АЧХ
plt.figure()
N = len(values)
FD = ORIGIN_FREQUENCY
myfuncSpectrOrig = np.fft.fft(values)
plt.stem(np.fft.fftfreq(N, 1./FD), np.abs(myfuncSpectrOrig/N), 'b') #график
plt.xlabel('Частота, Гц')
plt.ylabel('Амплитуда  сигнала')
plt.grid(True)

plt.figure()
myfuncSpectrDiscr = np.fft.fft(discretValues)
plt.stem(np.fft.fftfreq(len(discretValues), 1./DISCRET_FREQUENCY), np.abs(myfuncSpectrDiscr/N), 'g')
plt.grid(True)
plt.xlabel('Частота, Гц')
plt.ylabel('Амплитуда  сигнала')

plt.figure()
myfuncSpectrVost = np.fft.fft(newValues)
plt.stem(np.fft.fftfreq(N, 1./FD), np.abs(myfuncSpectrVost/N), 'r')
plt.grid(True)
plt.xlabel('Частота, Гц')
plt.ylabel('Амплитуда  сигнала')

# ФЧХ
plt.figure()
myfuncSpectrPhase = np.angle(myfuncSpectrOrig)
freq = np.fft.fftfreq(N)
plt.stem(freq,  myfuncSpectrPhase,'b')
plt.xlabel('Частота, Гц')
plt.ylabel('Фаза')
plt.grid(True)

plt.figure()
myfuncSpectrPhase = np.angle(myfuncSpectrDiscr)
plt.stem(np.fft.fftfreq(len(discretValues)), myfuncSpectrPhase,'g')
plt.grid(True)
plt.xlabel('Частота, Гц')
plt.ylabel('Фаза')

plt.figure()
myfuncSpectrPhase = np.angle(myfuncSpectrVost)
plt.stem(np.fft.fftfreq(N), myfuncSpectrPhase,'r')
plt.grid(True)
plt.xlabel('Частота, Гц')
plt.ylabel('Фаза')

plt.figure()
plt.plot(discretTimePoints, discretValues, 'g')
plt.xlabel('Время, сек')
plt.ylabel('Амплитуда сигнала')
plt.title('Дискретный сигнал')
plt.grid()

plt.show()
